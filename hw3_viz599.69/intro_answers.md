Questions:
1. If we were interested in understanding summary statistics at a glance (minimum, maximum, etc.), how could you improve the display of the table?

I would improve the display of the table by simply adding more data to it. Rank and rate mean nothing to me without any other substantial data.  Why does a state reach a certain rank in comparison to others?  Other quantitive data points being:
    a. state population
    b. state population density per square mile
    c. jobs created per year
    d. jobs lost per year etc.

2. Find three varied examples on the web that present tabular data in a different layout or in more visually compelling, complete, interactive or interesting ways.
    1. https://developers.google.com/chart/interactive/docs/examples#table-example
    2. https://constructive.co/insights/6-best-data-visualization-tools-2016-pt-1/
    3. https://www.ncsu.edu/labwrite/res/gh/gh-tables.html
