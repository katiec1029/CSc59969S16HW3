1. What does the colspan="3" attribute of the <th> node do?

    colspan defines the number of columns a cell should span

2. List all the styles (e.g. border width, text alignment, etc.) applied to the th element containing "Rank". For each, state whether they are set as an HTML attribute or a CSS style and describe them in a few words. Include only styles directly applied to the element, not styles inherited/cascading from parent elements or styles from the default user agent stylesheet. Exclude overwritten styles. For HTML attributes, state the CSS equivalent.

    According to the DOM inspector the style elements applied to the th element containing "Rank" were:
        1. align = "center" -> text align center in the table heading

3. What differences do you notice between the DOM inspector and the HTML source? Why would you use the DOM inspector? Why is the HTML source useful?
    DOM Inspector:
        1. able to easily navigate through the webpage to find the sought element
            -clearly organized
            -highlights element in which user is seeing source code
    HTML Source:
        1. seeing direct source
        2. able to determine logic coder used to produce final product

    I prefer using DOM Inspector because it makes it easier for me to read the source code and
    search for certain elements
